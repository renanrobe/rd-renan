import React, { Component } from 'react';
import LogoImg from '../../assets/logo.png';

import {
  HeaderWrapper,
  Logo,
  Menu,
  MenuItem
} from './Header.style'

class Header extends Component {
  render() {
    return <HeaderWrapper>
      <Logo>
        <a href="/" title="Raia Drogasil">
          <img src={LogoImg} alt="Raia Drogasil" />
        </a>
      </Logo>
      <Menu>
        <ul>
          <MenuItem><a href="#HTML5">HTML5</a></MenuItem>
          <MenuItem><a href="#CSS3">CSS3</a></MenuItem>
          <MenuItem><a href="#JAVASCRIPT">JAVASCRIPT</a></MenuItem>
          <MenuItem><a href="#REACT">REACT</a></MenuItem>
          <MenuItem><a href="#REDUX">REDUX</a></MenuItem>
        </ul>
      </Menu>
    </HeaderWrapper>;
  }
}

export default Header;