import styled from 'styled-components';

export const HeaderWrapper = styled.header`
  border-bottom: 1px solid ${({ theme }) => theme.colors.greenLight};
  display: flex;
  justify-content: space-between;
  width: 100%;
`
export const Logo = styled.h1`
  margin: 0;
  padding: 0;
`
export const Menu = styled.nav`
  align-items: center;
  display: flex;
  
  > ul {
    display: flex;
  }
`
export const MenuItem = styled.li`
  margin-right: 20px;

  > a {
    color: ${({ theme }) => theme.colors.green};
    font-family: ${({ theme }) => theme.FontFamily};
    font-size: 22px;
    font-weight: 300;

    :hover {
      color: ${({ theme }) => theme.colors.red};
      text-decoration: underline;
    }
  }
`