import React, { Component } from 'react';
import LogoRaia from '../../assets/logo_drogaraia.png';
import LogoDrogasil from '../../assets/logo_drogasil.png';
import LogoFarmasil from '../../assets/logo_farmasil.png';
import LogoUnivers from '../../assets/logo_univers.png';
import LogoBio from '../../assets/logo_4bio.png';
import LogoP from '../../assets/logo_small.png';

import {
  FooterWrapper,
  Copyright,
  Menu,
  MenuItem,
  LogoSmall
} from './Footer.style'

class Footer extends Component {
  render() {
    return <FooterWrapper>
      <div>
        <Copyright>RD 2017. Todos os direitos reservados</Copyright>
        <Menu>
          <ul>
            <MenuItem>
              <a href="#drogaraia" title="Droga Raia">
                <img src={LogoRaia} alt="Droga Raia" />
              </a>
            </MenuItem>
            <MenuItem>
              <a href="#drogasil" title="Drogasil">
                <img src={LogoDrogasil} alt="Drogasil" />
              </a>
            </MenuItem>
            <MenuItem>
              <a href="#farmasil" title="Farmasil">
                <img src={LogoFarmasil} alt="Farmasil" />
              </a>
            </MenuItem>
            <MenuItem>
              <a href="#univers" title="Univers">
                <img src={LogoUnivers} alt="Univers" />
              </a>
            </MenuItem>
            <MenuItem>
              <a href="#4bio" title="4Bio">
                <img src={LogoBio} alt="4Bio" />
              </a>
            </MenuItem>
          </ul>
        </Menu>
      </div>
      <LogoSmall>
        <a href="/"><img src={LogoP} alt="RaiaDrogasil" /></a>
      </LogoSmall>
    </FooterWrapper>;
  }
}

export default Footer;