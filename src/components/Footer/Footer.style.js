import styled from 'styled-components';

export const FooterWrapper = styled.footer`
  align-items: center;
  display: flex;
  justify-content: space-between;
  padding-bottom: 20px;
  width: 100%;

  > div {
    display: flex;
  }
`
export const Copyright = styled.p`
  color: ${({ theme }) => theme.colors.gray};
  font-family: ${({ theme }) => theme.FontFamily};
  font-size: 14px;
  font-weight: 300;
`
export const Menu = styled.nav`
  align-items: center;
  display: flex;
  margin-left: 50px;

  > ul {
    display: flex;
  }
`
export const MenuItem = styled.li`
  margin: 0 10px;
`
export const LogoSmall = styled.div`
  align-self: flex-end;
`