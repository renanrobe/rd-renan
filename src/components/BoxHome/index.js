import React, { Component } from 'react';
import ImgDesktop from '../../assets/desktop-responsive-design.png';
import ImgTablets from '../../assets/tablets-responsive-design.png';
import ImgMobile from '../../assets/mobile-responsive-design.png';

import {
  BoxHomeWrapper,
  BoxTop,
  Title,
  BoxBottom,
  Text,
} from './BoxHome.style'

const selectImg = img => {
  if(img === "img-desktop"){
    return ImgDesktop;
  }else if(img === "img-tablets"){
    return ImgTablets;
  }else if(img === "img-mobile"){
    return ImgMobile;
  }
}

class BoxHome extends Component {
  render() {
    return <BoxHomeWrapper>
      <BoxTop bgColor={this.props.item.Color}>
        <img src={selectImg(this.props.item.Img)} alt={this.props.item.Title} />
        <Title>{this.props.item.Title}</Title>
      </BoxTop>
      <BoxBottom bgColor={this.props.item.Color}>
        <Text>{this.props.item.Description}</Text>
        <a href="#action" onClick={ this.props.actionButton }>
          {this.props.item.TextButton}
        </a>
      </BoxBottom>
    </BoxHomeWrapper>;
  }
}

export default BoxHome;