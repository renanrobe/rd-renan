import styled from 'styled-components';

export const BoxHomeWrapper = styled.div`
  margin: 0 15px;
  width: 100%;

  @media only screen and (max-width: 950px) {
    margin-bottom: 30px;
  }
`
export const BoxTop = styled.div`
  background: ${({ theme, bgColor }) => theme.colors[bgColor]};
  height: 245px;
  justify-content: center;
  text-align: center;

  > img {
    margin: 20px 0 40px 0;
  }
`
export const Title = styled.h2`
  color: ${({ theme }) => theme.colors.white};
  font-family: ${({ theme }) => theme.FontFamily};
  font-size: 28px;
  @media only screen and (max-width: 1200px) {
    font-size: 20px;
  }
  font-weight: 300;
  margin: 0;
  text-align: center;
`
export const BoxBottom = styled.div`
  background-color: ${({ theme }) => theme.colors.white};
  display: flex;
  flex-direction: column;
  padding: 10px 15px  20px 15px;
  
  > a {
    align-self: flex-end;
    background: ${({ theme, bgColor }) => theme.colors[bgColor]};
    color: ${({ theme }) => theme.colors.white};
    font-family: ${({ theme }) => theme.FontFamily};
    font-size: 18px;
    font-weight: 500;
    padding: 3px 5px;
    margin-top: 5px;
  }
`
export const Text = styled.p`
  color: ${({ theme }) => theme.colors.gray};
  font-family: ${({ theme }) => theme.FontFamily};
  font-size: 18px;
  font-weight: 300;
  margin: 0;
  text-align: left;
`
