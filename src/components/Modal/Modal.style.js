import styled from 'styled-components';

export const ModalWrapper = styled.footer`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`
export const Background = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: black;
  opacity: 0.5;
  top: 0;
  left: 0;
`
export const ContentWrapper = styled.div`
  background-color: ${({ theme }) => theme.colors.white};
  font-family: ${({ theme }) => theme.FontFamily};
  font-size: 14px;
  font-weight: 300;
  margin: 0 auto;
  min-height: 50%;
  padding: 20px;
  position: relative;
  top: 25%;
  width: 50%;
`
export const Content = styled.div`
  padding: 20px;
`
export const Close = styled.div`
  cursor: pointer;
  font-size: 30px;
  font-weight: 300;
  position: absolute;
  right: 10px;
  top: 10px;

  :before {
    content: "x";
    display: block;
    height: 20px;
    width: 20px;
  }
`