import React, { Component } from 'react';

import {
  ModalWrapper,
  ContentWrapper,
  Background,
  Content,
  Close
} from './Modal.style'

class Modal extends Component {
  render() {
    return <ModalWrapper>
      <Background />
      <ContentWrapper>
        <Close onClick={ this.props.hideModal }/>
        <Content>
          {this.props.content}
        </Content>
      </ContentWrapper>
    </ModalWrapper>;
  }
}

export default Modal;