import React, { useState, useRef } from 'react';
import Header from './components/Header';
import Home from './pages/Home';
import Footer from './components/Footer';
import Modal from './components/Modal';
import { lightTheme, darkTheme } from "./Theme";
import { ThemeProvider } from "styled-components";

import './App.css';

function App() {
  const [theme, setTheme] = useState('light');
  const toggleTheme = () => {
    if (theme === 'light') {
      setTheme('dark');
    } else {
      setTheme('light');
    }
  }

  const [openModal, setOpenModal] = useState(false);
  const [contentModal, setContentModal] = useState("");
  const showModal = text => {
    setOpenModal(true);
    setContentModal(text);
  }
  const hideModal = () => {
    setOpenModal(false);
  }

  const myRefScrollDown = useRef(null);
  const scrollToRef = (ref) => window.scrollTo({
    top: ref.current.offsetTop,
    left: 0,
    behavior: 'smooth'
  });

  const [scrollDown, setScrollDown] = useState(false);
  const [contentScrollDown, setContentScrollDown] = useState("");
  const openContent = text => {
    setScrollDown(true);
    setContentScrollDown(text);
    scrollToRef(myRefScrollDown)
  }
  
  return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
      <div className="App">
        {openModal &&
          <Modal content={contentModal} hideModal={hideModal} />
        }
        <Header />
        <Home toggleTheme={toggleTheme} showModal={showModal} scrollDown={openContent} />
        <div ref={myRefScrollDown}>
          {scrollDown &&
            <p>
              {contentScrollDown}
              <br /><br />
              {contentScrollDown}
              <br /><br />
              {contentScrollDown}
            </p>
          }
        </div>
        <Footer />
      </div>
    </ThemeProvider>
  );
}

export default App;
