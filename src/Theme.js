export const lightTheme = {
  colors: {
    greenLight: '#E0E0E0',
    green: '#007f56',
    gray: '#868686',
    red: '#FE9481',
    yellow: '#FCDA92',
    purple: '#9C8CB9',
    white: '#FFFFFF'
  },
  FontFamily: 'Open Sans, Helvetica, sans-serif, arial'
};

export const darkTheme = {
  colors: {
    greenLight: '#333333',
    green: '#333333',
    gray: '#333333',
    red: '#333333',
    yellow: '#333333',
    purple: '#333333',
    white: '#FFFFFF'
  },
  FontFamily: 'Open Sans, Helvetica, sans-serif, arial'
};