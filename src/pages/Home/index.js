import React, { Component } from 'react';
import BoxHome from '../../components/BoxHome';

import mockContent from './MockContent.json';

import {
  HomeWrapper,
  BoxHomeWrapper,
  Title,
  Text,
  ListColors,
  ColorItem
} from './Home.style'

class Home extends Component {
  actionButton = (action, item) => {
    if(action === "scrollDown") {
      return this.props.scrollDown(item.Text);
    } else if(action === "showModal") {
      return this.props.showModal(item.Text)
    } else if(action === "changeTheme"){ 
      return this.props.toggleTheme()
    }
  }

  render() {
    return <HomeWrapper>
      <Title>Crie este site <span>responsivo</span> com <span>REACT</span> utilizando <span>styled-components</span></Title>
      <Text>
        A fonte utilizada é a Open Sans de 300 a 800.<br />
        exemplo: "Open Sans", Helvetica, sans-serif, arial;<br />
        Já as cores são:
      </Text>
      <ListColors>
        <ColorItem color="green">#007f56,</ColorItem>
        <ColorItem color="gray">#868686,</ColorItem>
        <ColorItem color="red">#FE9481,</ColorItem>
        <ColorItem color="yellow">#FCDA92 e</ColorItem>
        <ColorItem color="purple">#9C8CB9</ColorItem>
      </ListColors>
      <BoxHomeWrapper>
        { mockContent.map((item) =>
          <BoxHome
            key={item.id}
            item={item}
            actionButton={() => this.actionButton(item.Action, item)}
          />) }
      </BoxHomeWrapper>
    </HomeWrapper>;
  }
}

export default Home;