import styled from 'styled-components';

export const HomeWrapper = styled.div`
  text-align: center;
  width: 100%;
`
export const BoxHomeWrapper = styled.div`
  display: flex;
  margin: 40px auto;
  width: 95%;

  @media only screen and (max-width: 950px) {
    display: block;
    padding: 0 40px;
    box-sizing: border-box;
  }
`
export const Title = styled.h2`
  color: ${({ theme }) => theme.colors.green};
  font-family: ${({ theme }) => theme.FontFamily};
  font-size: 60px;
  font-weight: 300;

  > span {
    font-weight: 800;
  }
`
export const Text = styled.p`
  color: ${({ theme }) => theme.colors.gray};
  font-family: ${({ theme }) => theme.FontFamily};
  font-size: 32px;
  font-weight: 300;
  margin-bottom: 0;
`
export const ListColors = styled.ul`
  display: flex;
  justify-content: center;
`
export const ColorItem = styled.li`
  align-items: center;
  color: ${({ theme }) => theme.colors.gray};
  display: flex;
  font-family: ${({ theme }) => theme.FontFamily};
  font-size: 32px;
  font-weight: 300;
  margin-right: 10px;

  &:before {
    background-color: ${({ theme, color }) => theme.colors[color]};
    border-radius: 50%;
    content: '';
    display: block;
    height: 15px;
    width: 15px;
  }
`
